# Introduction
This is an example about how to develop an easy Jenkins Shared library giving some tips.

This document is oriented to **declarative pipelines**.

## Mandatory reading
* [Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/)
* [Jenkins Shared Library](https://jenkins.io/doc/book/pipeline/shared-libraries/)

# The Pipeline 

```
#!groovy
@Library('jenkins-sharedlibs@master') _

pipeline {
    agent {
        docker { image 'node:8.4.0' }
    } 
    stages {
        stage('Test Jenkins sharedlib') {
            steps {
				script { 
					semver() 
				}
            }
        }		
    }
}
```

# What on the hell is that "_"?
The analog to "_" is "import * from jenkins-sharedlibs". When using _ there's no need to add the sentence import __xxxx.xxxx.xxx__.

# How do I call my Shared Library methods?
They must be inside a "script" block.
```
stage('Test Jenkins sharedlib') {
	steps {
		script { 
			semver() // this is a custom method
		}
	}
}		
```

# Vars are like singletons
Jenkins defintion is "Define global variables accessible from Pipeline". 

When we work with a declarative pipeline, this is the easiest way to call our custom methods. It works like a singleton.

The groovy file contains a **call** method which is called by the Pipeline.

# How can I use script methods (sh, echo)
If your "vars" groovy files are plain methods and don't define a "class" inside. Your method are going to be able to see script's global vars like "sh" or "echo".

**echo**
> 
> Remember, **println** won't work in your groovy files; you have to use "echo".
> 
```
echo(tags.toString())
```

**How can I save the output of a command to a variable?**
```
def tags = sh (script: 'git tag -l', returnStdout: true).trim().tokenize('\n')
```

