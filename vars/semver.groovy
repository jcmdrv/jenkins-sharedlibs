#!/usr/bin/env groovy

def call() {
	def tags = sh (script: 'git tag -l', returnStdout: true).trim().tokenize('\n')

	echo(tags.toString())

	def tag = tags[tags.size() - 1]
	def tag_parts = tag.split('\\.')

	echo(tag_parts.toString())

	def patch = tag_parts[2]
	patch = patch.toInteger() + 1

	def new_version = tag_parts[0] + '.' + tag_parts[1] + '.' + patch

	echo(new_version)
	
	return new_version

}